(function () {
  let canvas = document.querySelector('canvas');
  if (!canvas) return;

  console.log('INIT shapes');

  let ctx = canvas.getContext('2d');



  let img = canvas.querySelector('img');


  img.onload = function () {

    shapes.titer(ctx);

  };


  let shapes = {
    titer(ctx) {
      let grd = ctx.createLinearGradient(0, 0, 1000, 1000);
      grd.addColorStop(0, '#005bb6');
      grd.addColorStop(1, '#4ca5e0');

      ctx.save();

      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 2;
      ctx.shadowBlur = 60;
      ctx.shadowColor = 'rgb(0, 0, 0, 0.1)';

      ctx.beginPath();
      ctx.moveTo(577.83264,451.32638);
      ctx.bezierCurveTo(549.54694,487.95691999999997,468.15004,480.91668999999996,404.51113999999995,499.2734);
      ctx.bezierCurveTo(341.02813999999995,517.74365,295.39423999999997,561.4376,240.69993999999994,565.69778);
      ctx.bezierCurveTo(185.84933999999993,569.84249,121.97293999999994,534.69324,79.06338399999993,476.17942999999997);
      ctx.bezierCurveTo(36.12063399999993,417.52343999999994,14.302613999999934,335.61609,37.07298399999993,278.67157);
      ctx.bezierCurveTo(59.84452399999993,221.72772999999998,127.48423999999991,189.83238999999998,177.91203999999993,143.81333999999998);
      ctx.bezierCurveTo(228.30673999999993,97.65301399999998,261.57803999999993,37.19842699999998,304.44123999999994,28.509836999999976);
      ctx.bezierCurveTo(347.18143999999995,19.848908999999978,399.4773399999999,62.811597999999975,443.0325399999999,109.02107999999997);
      ctx.bezierCurveTo(486.58703999999994,155.23178999999996,521.3686399999999,204.54693999999995,551.31274,269.39844);
      ctx.bezierCurveTo(581.25594,334.25014,606.08384,414.55291,577.83164,451.3266);
      ctx.closePath();
      ctx.fillStyle = grd;
      ctx.fill();

      ctx.clip();

      ctx.globalAlpha = 0.61;

      ctx.drawImage(img, 0, 0);

      ctx.restore();


    }
  }


})();