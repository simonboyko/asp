(function () {
  if (!document.getElementsByClassName('projectNews__list')[0]) return;
  console.log('INIT project-news');

  let projectNews__list = tns({
    container: '.projectNews__list',
    slideBy: 'page',
    controlsContainer: '.projectNews__arrows',
    navPosition: 'bottom',
    // navContainer: '.projectNews__dots',
    items: 1,
    responsive: {
      980: {
        items: 4,
      },
      768: {
        items: 3,
      },
      480: {
        items: 2,
      },
      320: {
        items: 1,
      }
    }
  });

})();