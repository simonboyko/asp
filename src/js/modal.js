(function () {
  console.log('INIT modal');

  let openedModal = null;

  let links = document.querySelectorAll('[data-modal]');

  if (links.length === 0) {
    return false;
  }

  links.forEach(link => {
    link.addEventListener('click', clickHandler);
  });


  function clickHandler(e) {
    console.log('f: clickHandler');
    e.preventDefault();

    if (openedModal !== null) {
      openedModal = null;
    }

    let link = e.currentTarget;
    openedModal = document.querySelector(link.getAttribute('data-modal'));

    console.log(e.currentTarget.href);
    openModal(e.currentTarget.href);
  }

  function openModal(src) {
    console.log('f: openModal');

    openedModal.classList.add('modal--opened');
    document.body.classList.add('noScroll');

    let iframe = openedModal.querySelector('.modal__video');
    if (iframe && iframe.getAttribute('src') === '') {
      iframe.setAttribute('src', `https://www.youtube.com/embed/${getVideoID(src)}?enablejsapi=1`);
    }


    let closers = openedModal.querySelectorAll('.modal__bg, .modal__close');
    closers.forEach(item => {
      item.addEventListener('click', closeModal);
    });

  }

  function closeModal() {
    console.log('f: closeModal');

    openedModal.classList.remove('modal--opened');
    document.body.classList.remove('noScroll');

    let iframe = openedModal.querySelector('.modal__video');
    if (iframe) {
      iframe.setAttribute('src', '');
    }

    try {
      openedModal.querySelector('.modal__video')
        .contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
    } catch (e) {}

    openedModal = null;
  }


  function getVideoID(url) {
    let id = 'r3Fg5oxYLfI';

    if (url.match(/youtu\.be/gi)) {
      // для ссылки вида https://youtu.be/5gXwGfAo1q8
      id = url.split('youtu.be/')[1];

    } else if (url.match(/youtube\.com\/watch/gi)) {
      // для ссылки вида https://www.youtube.com/watch?v=5gXwGfAo1q8
      id = url.split('watch?v=')[1];
    } else if (url.match(/youtube\.com\/embed/gi)) {
      // для ссылки вида https://www.youtube.com/embed/Gl7NsSmT26Q
      id = url.split('/embed/')[1];
      console.log('embed');
    } else {
      console.log('Неправильная ссылка на видео!');
    }

    console.log(id);
    return id;
  }

})();