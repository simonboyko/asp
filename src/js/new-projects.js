(function () {
  if (!document.getElementsByClassName('newProjects__list')[0]) return;
  console.log('INIT new-projects');


  let slider = tns({
    container: '.newProjects__list',
    slideBy: '1',
    controls: false,
    autoplay: false,
    nav: false,
    autoplayButtonOutput: false,
    items: 2,
    responsive: {
      980: {
        items: 3,
      },
      768: {
        disable: false,
      },
      0: {
        disable: true,
      }
    }
  });

  customAutoplay(8000);

  slider.events.on('transitionEnd', pulseFirstItem);
  slider.events.on('transitionStart', removeOldPulse);


  function pulseFirstItem(e) {
    let parent = e.container;

    let firstActiveSlide = parent.querySelectorAll('.tns-slide-active .newProjects__card')[0];

    firstActiveSlide.classList.add('newProjects__card--new');
  }

  function removeOldPulse(e) {
    let parent = e.container;

    let firstActiveSlide = parent.querySelector('.newProjects__card--new');

    if (firstActiveSlide) {
      firstActiveSlide.classList.remove('newProjects__card--new');
    }

  }

  function customAutoplay(timeout) {
    setInterval(function () {
      slider.goTo('prev');
    }, timeout)
  }

})();