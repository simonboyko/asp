(function () {
  console.log('INIT mob-menu');

  let button = document.querySelector('.mobMenu__button');
  let menu = document.querySelector('.mobMenu');
  let opened = false;
  let header = document.querySelector('.header');

  button.addEventListener('click', clickHandler);

  function openMenu() {
    console.log('f: openMenu');

    opened = true;
    button.classList.add('mobMenu__button--opened');
    menu.classList.add('mobMenu--opened');
    document.body.classList.add('noScroll');
    header.classList.add('header--mobMenu');
  }

  function closeMenu() {
    console.log('f: closeMenu');

    opened = false;
    button.classList.remove('mobMenu__button--opened');
    menu.classList.remove('mobMenu--opened');
    document.body.classList.remove('noScroll');
    header.classList.remove('header--mobMenu');
  }

  function clickHandler(e) {
    console.log('f: clickHandler');

    if (!opened) {
      openMenu();
    } else {
      closeMenu();
    }
  }

})();