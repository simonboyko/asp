(function () {
  console.log('INIT smooth-scroll');

  let scroll = new SmoothScroll('[data-scroll]');

  let anchorLinks = document.querySelectorAll('[data-scroll]');
  anchorLinks.forEach(link => replaceHrefIfNeed(link));

  function replaceHrefIfNeed(link) {
    let elId = link.href.split('#')[1];

    if (document.getElementById(elId)) {
      link.href = '#' + link.href.split('#')[1];
    }
  }
})();