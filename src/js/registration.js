(function () {
  let forms = document.querySelectorAll('.form[data-ajax-form]');
  if (forms.length === 0) return;

  console.log('INIT registration');


  let url = '/wp-json/api/reg';
  
  forms.forEach(form => {
    form.addEventListener('submit', sendForm);
  });
  function sendForm(e) {
    console.log('f: sendForm');
    e.preventDefault();

    let data = {
      log: e.srcElement[0].value,
      email: e.srcElement[1].value,
      role: e.target.getAttribute('data-role')
    };

    axios({
      method: 'post',
      url: url,
      data: data
    })
      .then(res => {
        console.log(res);
        if (res.data.status === true) {
          addResponseForForm(e.target, res.data.text);
        } else {
          addError(e.target, res.data.text)
        }
      })
      .catch(e => console.log(e));



  }

  function addResponseForForm(form, res) {
    let wrp = customClosest(form, '.cntWrp');


    // if (form.getAttribute('data-role') === 'project_leader') {
    //   res = '<h2 class="regProject__heading block__heading">Вы успешно зарегистрировались!</h2><p class="regProject__dscr block__dscr">Мы направили пароль на почту, указанную при регистрации.</p><p class="regProject__body">Для заполнения паспорта проекта нажмите кнопку ниже.</p><a class="button regProject__button" href="#0">Заполнить информацию о проекте</a>'
    // } else if (form.getAttribute('data-role') === 'project_tracker') {
    //   res = '<h2 class="modal__title">Вы успешно зарегистрировались!</h2><p class="modal__dscr">Мы направили пароль на почту, укрегистрации.<br>Для заполнения профиля нажмите кнопку ниже.<p><a class="button modal__button" href="#0">Заполнить профиль</a>';
    // } else {
    //   return false;
    // }

    wrp.innerHTML = res;

  }

  function addError(form, text) {

    try {
      form.querySelector('.form__error').innerHTML = text;
      return;
    } catch {}

    let err = document.createElement('div');
    err.className = 'form__error';
    err.innerHTML = text;

    form.appendChild(err);

  }

  function customClosest(el, sel) {
    while ((el = el.parentElement) && !((el.matches || el.matchesSelector).call(el,sel)));
    return el;
  }
  
})();