(function () {
  console.log('INIT all-project');

  if (!document.querySelector('.projectsFilter')) return;


  let ProjectsAJAX = function () {

    let url = '/wp-json/api/projects';
    let currentPage = 1;
    let maxPages = null;
    let btn = document.querySelector('.projects__button');
    let listCnt = document.querySelector('.projects__list');
    let categories = document.querySelectorAll('.projectsFilter__checkbox');
    let searchField = document.querySelector('.projectsFilter__search');



    // кнопка "Показать еще"

    btn.addEventListener('click', function () {
      this.loadMore();
    }.bind(this));
    this.loadMore = function (e) {
      console.log('f: loadMore');

      startBtnLoader();

      if (currentPage < maxPages) {

        currentPage++;

        loadItems(currentPage)
          .then(res => {

            addHTML(res.html);
            stopBtnLoader();

          }).catch(e => console.log(e))

      } else {
        console.log('Данных для загрузки больше нет!');
      }

    };




    // фильтры категорий

    categories.forEach(cat => cat.addEventListener('change', function (e) {
      let id = e.target.value;
      let action = e.target.checked ? 'add' : 'remove';

      this.filterCategory(id, action);

    }.bind(this)));
    let categoriesIdList = [];
    this.filterCategory = function (id, action) {

      startListLoader();

      id = Number(id);
      currentPage = 1;

      if (action === 'add') categoriesIdList.push(id);

      if (action === 'remove') {
        categoriesIdList.pop(categoriesIdList.indexOf(id));
      }

      loadItems(1, categoriesIdList)
        .then(res => {

          replaceHTML(res.html);
          stopListLoader();

        })

    };





    // поиск

    searchField.addEventListener('input', debounce(function (e) {
      filterBySearch(e.target.value);
    }, 300));
    function filterBySearch(str, hide) {

      startListLoader();

      console.log('f: filterBySearch: ' + str);

      currentPage = 1;
      handleClearSearch(str);

      loadItems(null,categoriesIdList,str)
        .then(res => {

          replaceHTML(res.html);
          toggleButton();
          stopListLoader();
          showFound((!str || hide) ? null : res.html, str);

        }).catch(e => console.log(e));
    };

    let cleaner = document.createElement('div');
    cleaner.className = 'projectsFilter__searchCleaner';
    cleaner.addEventListener('click', function () {
      searchField.value = '';
      filterBySearch('', true);
    });

    function handleClearSearch(str) {
      if (str && !cleaner.parentElement) {
        searchField.parentElement.appendChild(cleaner);
      }
      if (!str && cleaner.parentElement) {
        searchField.parentElement.removeChild(cleaner);
      }
    }


    let foundList = document.createElement('div');
    foundList.className = 'projectsFilter__foundList';

    function showFound(html, str) {

      if (!html) {
        if (foundList.parentNode) searchField.parentElement.removeChild(foundList);
        return;
      }

      let code = document.createElement('div');
      code.innerHTML = html;

      let titles = code.querySelectorAll('.projects__title');

      foundList.innerHTML = '';
      titles.forEach(title => {
        let textValue = title.innerHTML.trim().toLowerCase();
        let textShown = textValue.replace(str, `<strong>${str}</strong>`);

        foundList.innerHTML = foundList.innerHTML + `<div class="projectsFilter__foundListItem" data-value="${textValue}" tabindex="1">${textShown}</div>`;
      });

      if (!foundList.parentNode) {
        searchField.parentElement.appendChild(foundList);


        let listItems = foundList.querySelectorAll('.projectsFilter__foundListItem');

        listItems.forEach(item => item.addEventListener('click', function (e) {
            searchField.value = e.target.getAttribute('data-value');
            searchField.parentElement.removeChild(foundList);
            filterBySearch(e.target.getAttribute('data-value'), true);
          }));

        // let titleByFocus = -1;
        // document.body.addEventListener('keyup', function (e) {
        //
        //   if (e.keyCode === 40) {
        //     if (document.querySelector('.projectsFilter__foundListItem--hover')) {
        //       document.querySelector('.projectsFilter__foundListItem--hover').classList.remove('projectsFilter__foundListItem--hover');
        //     }
        //   }
        //
        //   if (e.keyCode === 40 || e.keyCode === 38) {
        //
        //   }
        //
        //   // if (document.querySelector('.projectsFilter__foundListItem--hover')) {
        //   //   document.querySelector('.projectsFilter__foundListItem--hover').classList.remove('projectsFilter__foundListItem--hover');
        //   // }
        //
        //   return titleByFocus < listItems.length - 1 ? titleByFocus++ : null;
        // });

      }

    }

    document.body.addEventListener('click', function () {
      return foundList.parentElement ? searchField.parentElement.removeChild(foundList) : false;
    });








    // загружаем данные с сервера по параметрам
    function loadItems(page, category, search) {

      let params = '?';
      if (page) params = `${params}paged=${page}&`;
      if (category && category.length !== 0) params = `${params}categories=${category}&`;
      if (search) params = `${params}search=${search}&`;
      params = params.replace(/&$/, '');

      return axios.get(url + params)
        .then(res => {
          maxPages = +res.data.max_num_pages;
          toggleButton();
          return res.data;
        })
        .catch(e => console.error(e));

    };


    // добавляем новые карточки проектов
    function addHTML(html) {
      listCnt.innerHTML = listCnt.innerHTML + html;
    };


    // полностью заменяем карточки проектов
    function replaceHTML(html) {
      listCnt.innerHTML = html;
    };


    // прелоадер для кнопки
    function startBtnLoader() {
      btn.classList.add('projects__button--loader');
    };
    function stopBtnLoader() {
      btn.classList.remove('projects__button--loader');
    };


    // прелоадер для карточек
    function startListLoader() {
      listCnt.classList.add('projects__list--loader');
    };
    function stopListLoader() {
      listCnt.classList.remove('projects__list--loader');
    };


    // узнаем и устанавливаем максимальное количество страниц
    function initPages() {
      currentPage = 1;
      maxPages = +btn.getAttribute('data-pages');
      toggleButton();
    };
    initPages();


    // показываем/скрываем кнопку когда надо
    function toggleButton() {
      if (currentPage >= maxPages) {
        btn.style.display = 'none';
      } else {
        btn.style.display = '';
      }
    }


    function debounce(f, ms) {

      let timer = null;

      return function (...args) {
        const onComplete = () => {
          f.apply(this, args);
          timer = null;
        };

        if (timer) {
          clearTimeout(timer);
        }

        timer = setTimeout(onComplete, ms);
      };
    }

  };

  let prj = new ProjectsAJAX();

})();