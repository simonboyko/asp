(function () {
  console.log('INIT select');

  let projectTypeSelect = document.querySelector('.iCreate__select');
  if (!projectTypeSelect) return;

  let choices = new Choices(projectTypeSelect, {
    searchEnabled: false,
    shouldSort: false,
    itemSelectText: '',
    addItemText: ''
  });

  console.log(choices.passedElement);

  projectTypeSelect.addEventListener('change', choicesHandler);
  
  function choicesHandler(e) {
    let value = e.target.value;

    toggleAnswer(value);
    filter(value);
  }

  function toggleAnswer(value) {
    let currentActive = document.querySelector('.iCreate__answer:not([hidden])');
    if(currentActive) {
      currentActive.hidden = true;
    }

    document.querySelector(`.iCreate__answer--${value}`)
      .hidden = false;
  }


  function filter(type) {
    // скрываем видимые элементы
    document.querySelectorAll('.howToList__item:not([hidden])')
      .forEach(i => i.hidden = true);

    // показываем нужны
    document.querySelectorAll(`.howToList__item--${type}`)
      .forEach(i => i.hidden = false);

  }
  
  // Показываем в самом начале только те проекты, которые нужны
  filter(choices.getValue().value);
  toggleAnswer(choices.getValue().value);

  
})();